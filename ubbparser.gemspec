Gem::Specification.new do |s|
  s.name        						= 'ubbparser'
  s.version     						= '0.3.3'
  s.date        						= '2018-10-15'
  s.summary     						= 'UBB Parser'
  s.description 						= 'A simple and flexibel ubb parser.'
  s.authors     						= ['Taco Jan Osinga']
  s.email       						= 'info@osingasoftware.nl'
  s.files       						= ['lib/ubbparser.rb']
  s.homepage    						= 'https://github.com/tjosinga/UBBParser'
	s.license                 = 'MIT'
end